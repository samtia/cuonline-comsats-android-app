package falcons.atiqsamtia.cuonline;

/**
 * Created by Atiq on 2/16/2016.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


public class Our_View extends WebViewClient {

    protected Context cont;

    public Our_View(Context c){
        cont = c;
    }

    private ProgressDialog progressDialog;
    @Override
    public boolean shouldOverrideUrlLoading(WebView v, String url)
    {
        if (url.startsWith("tel:")) {
            Intent intent = new Intent(Intent.ACTION_DIAL,
                    Uri.parse(url));
            cont.startActivity(intent);
            return true;
        }
        if (url.startsWith("mailto:")) {

            MailTo mt = MailTo.parse(url);
            Intent i = newEmailIntent(cont, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
            cont.startActivity(i);
            CampusWebView.my_browse.reload();
            return true;
        }

        if (url.startsWith("https://play.google.com/store/")) {

            final String appPackageName = cont.getPackageName(); // getPackageName() from Context or Activity object
            try {
                cont.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                cont.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
            }

            return true;
        }


        // Toast.makeText(MainActivity.act,url,Toast.LENGTH_LONG).show();
        v.loadUrl(url);
        return true;
    }


    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);

        CampusWebView.my_browse.loadUrl("about:blank");
        //    MainActivity.NoInternet();

        AlertDialog.Builder builder = new AlertDialog.Builder( CampusWebView.act);
        builder.setMessage("Your internet connection is down. Please Connect internet connection and Launch Application Again.\n" +
                "Thanks")
                .setTitle("Not Connected!")
                .setCancelable(false)
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        CampusWebView.act.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        //  view.loadUrl("about:blank");
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon){
        // Check to see if there is a progress dialog
        if (progressDialog == null) {
            // If no progress dialog, make one and set message
            progressDialog = new ProgressDialog(CampusWebView.act);
            progressDialog.setMessage("Loading please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            // Hide the webview while loading
            view.setEnabled(false);
        }
    }


    @Override
    public void onPageFinished(WebView view, String url) {
        // Page is done loading;
        // hide the progress dialog and show the webview

        try {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
                view.setEnabled(true);
            }
        } catch(Exception ex){

        }
    }


    private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
    }


}






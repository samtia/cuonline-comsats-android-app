package falcons.atiqsamtia.cuonline;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class CampusWebView extends AppCompatActivity {


    public static Activity act;
    public static WebView my_browse;
    boolean doubleBackToExitPressedOnce = false;

    private Tracker mTracker;
    String name = new String("CampusWebView");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        setContentView(R.layout.activity_web_view);



        CuOnlineApp application = (CuOnlineApp) getApplication();
        mTracker = application.getDefaultTracker();



        act = this;


        setTitle(extras.getString("campus"));

        name += " " + extras.getString("campus");

        if(!isNetworkAvailable()){

            NoInternet();


        } else {


            // Get tracker.
            Tracker t = ((CuOnlineApp) getApplication()).getDefaultTracker();

            // Build and send an Event.
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("CAMPUS_SELECTED")
                    .setAction("OPENED")
                    .setLabel(extras.getString("campus"))
                    .build());


            my_browse = (WebView) findViewById(R.id.webView);
            my_browse.getSettings().setJavaScriptEnabled(true);
            my_browse.getSettings().setLoadWithOverviewMode(true);
            my_browse.getSettings().setUseWideViewPort(true);
            my_browse.getSettings().setBuiltInZoomControls(true);
            my_browse.getSettings().setSaveFormData(true);
            my_browse.getSettings().setSupportZoom(true);
            my_browse.setWebViewClient(new Our_View(CampusWebView.this));
            my_browse.loadUrl(extras.getString("url"));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        //using tracker variable to set Screen Name
        mTracker.setScreenName(name);
        //sending the screen to analytics using ScreenViewBuilder() method
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }




    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce || !my_browse.canGoBack()) {
            finish();
            super.onBackPressed();
            return;
        }


        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again quickly to goto Home", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);


        if (my_browse.canGoBack()) {
            my_browse.goBack();
        } else {
            finish();
            super.onBackPressed();
        }
    }




    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static void NoInternet(){
        AlertDialog.Builder builder = new AlertDialog.Builder( CampusWebView.act);
        builder.setMessage("Your internet connection is down. Please Connect internet connection and Launch Application Again.\n" +
                "Thanks")
                .setTitle("Not Connected!")
                .setCancelable(false)
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        CampusWebView.act.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }


}

package falcons.atiqsamtia.cuonline;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;

public class CUOnline extends AppCompatActivity {

    private Tracker mTracker;
    String name = new String("Main Screen");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuonline);

        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.cu_back_top));

        CuOnlineApp application = (CuOnlineApp) getApplication();
        mTracker = application.getDefaultTracker();


    }


    @Override
    protected void onResume() {
        super.onResume();

        //using tracker variable to set Screen Name
        mTracker.setScreenName(name);
        //sending the screen to analytics using ScreenViewBuilder() method
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void islamabad(View v){
        StartView("Islamabad",getString(R.string.islamabad));
    }

    public void abbotabad(View v){

        StartView("Abbotabad",getString(R.string.abbotabad));
    }

    public void lahore(View v){

        StartView("Lahore",getString(R.string.lahore));
    }

    public void attock(View v){

        StartView("Attock",getString(R.string.attock));
    }

    public void wah(View v){

        StartView("Wah",getString(R.string.wah));
    }

    public void sahiwal(View v){

        StartView("Sahiwal",getString(R.string.sahiwal));
    }

    public void vehari(View v){

        StartView("Vehari",getString(R.string.vehari));
    }

    public void virtual(View v){

        StartView("Virtual",getString(R.string.virtual));
    }


    public void StartView(String campus,String url){


        // Get tracker.
        Tracker t = ((CuOnlineApp) getApplication()).getDefaultTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory("CAMPUS_SELECTED")
                .setAction("BUTTON_CLICKED")
                .setLabel(campus)
                .build());


        Intent i = new Intent(this,CampusWebView.class);
        i.putExtra("campus",campus);
        i.putExtra("url",url);
        startActivity(i);
    }









    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.about:

                startActivity(new Intent(this,About.class));
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}

package falcons.atiqsamtia.cuonline;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class About extends AppCompatActivity {

    private Tracker mTracker;
    String name = new String("About Screen");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);


        CuOnlineApp application = (CuOnlineApp) getApplication();
        mTracker = application.getDefaultTracker();


    }



    @Override
    protected void onResume() {
        super.onResume();

        //using tracker variable to set Screen Name
        mTracker.setScreenName(name);
        //sending the screen to analytics using ScreenViewBuilder() method
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
